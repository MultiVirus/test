$(document).ready(function(){
      $('.custom-slider').slick({
     arrows:false,
     autoplay:true,
     autoplaySpeed:2500,
     speed:2000,
     pauseOnFocus:false,
     pauseOnHover:false,
     pauseOnDotsHover:false,
     draggable:false,
      });
    });
$(function(){
      $('.long_slider').slick({
     arrows:false,
     slidesToShow:4,
     easing:'ease',
     autoplay:true,
     autoplaySpeed:3000,
     speed:500,
     responsive: [
    {
      breakpoint: 1280,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
      });
    });
$(function(){
      $('.slider_destination').slick({
     arrows:false,
     slidesToShow:3,
     speed:500,
     responsive: [
    {
      breakpoint: 1280,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
      });
    });
